# Gitlab Group Workflow
---
When i begin a project in gitlab with a new group, i saw that they are almost new to gitlab and they didn't know what and how to do. Then i decide to make a basic and simple workflow for them. 
## Step By Step workflow
---

### As an owner or master
+ **step-1:** Create a group 
+ **step-2:** Add group member and set their role
+ **step-3:** Create new project within the Group
+ **step-4:** Add project member and set their role
+ **step-5:** Create issue and assign them within the project member
+ **step-6:** Create milestones 
+ **step-7:** Create label
+ **step-8:** Merge new code or features according to the merge request

### As another member of the project

+ **step-9:** Fork or Download the repository for working with code
+ **step-10:** Create new feature branch and push it in your project repository
+ **step-11:** Create merge request.  
*you can also do other work like as master work according to your role set.*

## A brief discussion about gitlab
---

## Git and Gitlab
---

**Git** - A source code versioning system that lets you locally track changes and push or pull changes from remote resources.  

**GitLab** - A service that provides remote access to Git repositories. In addition to hosting your code, the services provide additional features designed to help manage the software development lifecycle. These additional features include managing the sharing of code between different people, bug tracking, wiki space and other tools for 'social coding'.
*or*  
GitLab, the software, is a web-based Git repository manager with wiki and issue tracking features.  

For read more about the workflow of Git [here](http://nvie.com/posts/a-successful-git-branching-model/)

## Gitlab Features
After **signin** with your gitlab account you may want to create a *git repository* or *project* using the **_NEW PROJECT_** button. Gitlab than give you the command line code or command for communicating with your local project.

Gitlab also give you the ability to keep your project **_private, public or internal_** when you create them. It also have a function to create group for working togather as a team. You can set permission for your team member in different role such as:
1. Guest -> a visitor with limited access
2. reporter -> a communicative observer
3. Developer -> the workforce
4. master -> powerful and in control  

For details [here](https://gitlab.com/help/permissions/permissions)

### Gitlab also provide other necessary functionality, When you working with source code.
---
<dl> 
	<dt>Commit messages</dt>
		<dd>Track the changes you made.</dd>
 	<dt>Comments</dt>
		<dd>Responds to any request or statement.</dd>
 	<dt>Issues</dt>
		<dd>Knowing what needs to be done.</dd>
 	<dt>Merge requests</dt>
		<dd>A request to merge code from another branch or fork.</dd>
 	<dt>Milestones</dt>
		<dd>Setting a due date.</dd>
 	<dt>Wiki pages</dt>
		<dd>Well define documentation for any feature with git power wiki.</dd>
</dl>

It important to refer to a line of code, a file, or other things, when discussing something.

The Above items (basically, prefixed strings or IDs) can be referenced through shortcodes:  

+ **@foo** for team members
+ **#123** for issues
+ **!123** for merge requests
+ **$123** for snippets
+ **1234567** for commits

Within a commit message you can refer a closed issue as **"closed or fixed #2"** means issue 2 is fixed.

## A brief discussion about gitlab functionality
---
### Issue
An issue is a text message of variable length, describing a bug in the code, an
improvement to be made, or something else that should be done or discussed. By
commenting on the issue, developers or project leaders can respond to this request
or statement. The meta information attached to an issue can be very valuable to the
team, because developers can be assigned to an issue, and it can be tagged or labeled
with keywords that describe the content or area to which it belongs. Furthermore,
you can also set a goal for the milestone to be included in this fix or feature.

### Label
Labels are tags used to organize issues by the topic and severity.
Creating labels is as easy as inserting them, separated by a comma, into the
respective field while creating an issue.It's also provide a certain background color for giving them an extra attention.

After the creation of a label, it will be listed under the Labels tab within the Issues
page, with a link that lists all the issues that have been labeled the same.

### Merge
A merge request is basically a request to merge code from another branch or fork.

GitLab can make use of this approach by displaying merge requests when a feature
or change is considered done and ready to be merged back into the main branch of
the project.

you may specify a title for the merge request, ideally a summary of the commits that are
being proposed as a change.A user can accept or deny the merge request, as also
set milestone for which version of your software the changes are targeted can be
selected. You can assign a user to do the desired merge.

### Milestones
Milestones allow you to organize issues and merge requests into a cohesive group, optionally setting a due date. A common use is keeping track of an upcoming software version. Milestones are created per-project.

### Fork
Developers with access to a repository can now copy a project to their own namespace and account to
work on the code.

## Reference
---
1. Gitlab repository management.
2. Gitlab Cookbook.
